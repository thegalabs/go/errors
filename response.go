package errors

import (
	"fmt"
	"net/http"
)

// Response is the default structure of an error response
type Response struct {
	StatusCode int    `json:"-"`
	Code       string `json:"code,omitempty"`
	Detail     string `json:"detail,omitempty"`
}

func (e Response) Error() string {
	return e.Code
}

// GetStatusCode returns the status code
func (e Response) GetStatusCode() int {
	if e.StatusCode == 0 {
		return http.StatusBadRequest
	}
	return e.StatusCode
}

// GetResponse returns the same object
func (e Response) GetResponse() interface{} {
	return e
}

// Detail generates a default error response with the detail set
func Detail(format string, args ...interface{}) error {
	return &Response{
		Detail: fmt.Sprintf(format, args...),
	}
}
