package errors

import (
	"fmt"
)

// NoResponse is a response with no body
type NoResponse int

// Error returns a string
func (e NoResponse) Error() string {
	return fmt.Sprintf("HTTP Error %d", e)
}

// GetStatusCode returns the status code
func (e NoResponse) GetStatusCode() int {
	return int(e)
}

// GetResponse returns the same object
func (e NoResponse) GetResponse() interface{} {
	return nil
}
