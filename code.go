package errors

import "net/http"

// Code is to make it easier to return a 400
type Code string

func (e Code) Error() string { return string(e) }

// GetStatusCode returns the status code
func (e Code) GetStatusCode() int {
	return http.StatusBadRequest
}

// GetResponse returns the same object
func (e Code) GetResponse() interface{} {
	return Response{Code: string(e)}
}
