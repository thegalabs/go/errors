package echo

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/rs/zerolog/log"

	"gitlab.com/thegalabs/go/errors"
)

// Handler returns an echo Error Handler
func Handler(translator errors.Translator) echo.HTTPErrorHandler {
	return func(err error, c echo.Context) {
		if c.Response().Committed {
			return
		}

		he := translator(err)
		if he == nil {
			_ = c.NoContent(http.StatusInternalServerError)
			log.Err(err).Msg("Internal Server Error")
			return
		}

		if body := he.GetResponse(); body == nil {
			err = c.NoContent(he.GetStatusCode())
		} else {
			err = c.JSON(he.GetStatusCode(), body)
		}

		if err != nil {
			log.Err(err).Msg("Could not send HTTPError")
		}
	}
}

// DefaultHandler uses the default translator
func DefaultHandler() echo.HTTPErrorHandler {
	return Handler(Translator)
}
